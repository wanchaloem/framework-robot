*** Settings ***
Resource          ../Keyword/AllResorces.robot
Resource          Variables.robot

*** Test Cases ***
TC01_WebTestingShop
    Set Library Search Order    SeleniumLibrary
    Log To Console    <<<< START >>>>
    Log in WebTestingShop
    ${Text1}    Get Text Page T-SHIRT    //h1[@class='h1']
    Log To Console    ${Text1}
    [W] Select From List By Value    //select[@id='group_1']    XL
    [W] Click Button    //*[@class="material-icons touchspin-up"]
    [W] Click Button    //*[@class="material-icons touchspin-up"]
    [W] Click Button    //*[@class="btn btn-primary add-to-cart"]
    Sleep    4
    ${Text2}    [M] Get Text    //p[@class='cart-products-count']
    Log To Console    ${Text2}
    [W] Click Element    //*[@class='btn btn-primary']
    Sleep    4
    [W] Click Button    //a[@class='btn btn-primary']
    Add Addresses
    [W] Click Button    confirmDeliveryOption
    [W] Click Element    //input[@id='payment-option-1']
    [W] Click Element    //*[@class="custom-checkbox"]
    [W] Click Element    //button[@class='btn btn-primary center-block']
    ${Text3}    [M] Get Text    //*[@class="h1 card-title"]
    Log To Console    ${Text3}
    Log To Console    <<<< END >>>>
    Close Browser

TC02_Robot Framework
    Set Library Search Order    SeleniumLibrary
    Log To Console    <<<< START >>>>
    Go to WebRobot    Robot Framework
    [W] Wait Until Page Contains    ROBOT FRAME WORK/
    ${Text}    Get Text Webrobot    //*[@class="main-header"]
    Log To Console    ${Text}
    Log To Console    <<<< END >>>>
    Close Browser

TC03_Introduction
    Set Library Search Order    SeleniumLibrary
    Log To Console    <<<< START >>>>
    @{ListData}    [Excel] Get Data All Row For Excal    TestData    TestData.xlsx    Sheet1    2
    Go to WebRobot by Headless Browser    @{ListData}[1]
    [W] Wait Until Page Contains    @{ListData}[2]
    ${Text}    Get Text Webrobot    //*[@class="col-md-5 col-lg-4 order-1 textblock-left p-3"]
    Log To Console    ${Text}
    Log result    ${Text}
    Log To Console    <<<< END >>>>
    Close Browser

TC04_GetDataDB
    Log To Console    <<<< START >>>>
    [DB] Connect to Database    customer_db    userroot    1234    127.0.0.1    3306
    ${list}    Query    SELECT * FROM `customer_profile`
    Log To Console    Data ที่มีอยู่แล้ว = ${list}
    [Excel] Log file Result    Testdata1    TestResult.xls    ${list}
    ${rowCount}    Row Count    select * from customer_profile where 1
    Log To Console    Row ALL =${rowCount}
    ${output}    Query    SELECT `name`, `age` FROM `customer_profile` WHERE `age` < "40"
    Log To Console    Name of age < 40 = ${output}
    ${output}    Query    SELECT `name`, `age` FROM `customer_profile` WHERE `age` >= "40"
    Log To Console    Name of age > 40 = ${output}
    Log To Console    <<<< END >>>>

TC05_InsertDataDB
    Log To Console    <<<< START >>>>
    [DB] Connect to Database    customer_db    userroot    1234    127.0.0.1    3306
    [DB] Insert Data to Database    Name02    Surname02    40    0987654321
    Log To Console    <<<< END >>>>

TC06_DeleteDataDB
    Log To Console    <<<< START >>>>
    [DB] Connect to Database    customer_db    userroot    1234    127.0.0.1    3306
    ${list}    [DB] Delete Data    name    name0
    Log To Console    ${list}
    Log To Console    <<<< END >>>>

TC07_GetAPI
    [Setup]
    Log To Console    <<<< START >>>>
    [API] GetAPI    ${URL_GET_API}    /api/users/2    Janet
    Log To Console    <<<< END >>>>

TC08_PostAPI
    [Setup]
    Log To Console    <<<< START >>>>
    [API] PostAPI    ${URL_POST}     eve.holt@reqres.in    cityslicka
    Log To Console    <<<< END >>>>
