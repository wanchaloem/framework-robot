*** Settings ***
Resource          ../AllKeywords.robot

*** Keywords ***
Go to WebRobot
    [Arguments]    ${Text}
    Set Library Search Order    SeleniumLibrary
    [W] Open Browser    https://www.google.com/
    [W] Input Text    name=q    ${Text}
    [W] Enter    name=q
    [W] Click Element    //*[@class="iUh30 tjvcx"]

Get Text Webrobot
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    SeleniumLibrary.Capture Page Screenshot
    ${Text}    [W] Get Text    ${Element}
    [Return]    ${Text}

Log result
    [Arguments]    ${Text Value}
    [W] Log file Result    Testdata1    TestResult.txt    ${Text Value}

Go to WebRobot by Headless Browser
    [Arguments]    ${Text}
    Set Library Search Order    SeleniumLibrary
    [W] Open Headless Browser    https://www.google.com/
    [W] Input Text    name=q    ${Text}
    [W] Enter    name=q
    [W] Click Element    //*[@class="iUh30 tjvcx"]
