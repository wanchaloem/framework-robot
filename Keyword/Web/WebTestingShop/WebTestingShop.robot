*** Settings ***
Resource          ../AllKeywords.robot

*** Keywords ***
Log in WebTestingShop
    Set Library Search Order    SeleniumLibrary
    [W] Open Browser    http://demoexex.esy.es/index.php?fbclid=IwAR0TbLizuCV2zd4bGq1K0i3zxSgGNS4f6xbE65Y9lXuki8TQibL4XtPGqXQ
    [W] Click Element    //span[contains(text(),'Sign in')]
    [W] Input Text    email    wanchaloem@cpru.ac.th
    [W] Input Text    password    0928297806
    [W] Click Button    submit-login

Get Text Page T-SHIRT
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    [W] Click Image    //*[@src="/img/logo.png"]
    [W] Click Image    //article[1]//div[1]//a[1]//img[1]
    [W] Wait Until Page Contains    Hummingbird printed t-shirt
    ${Text}    [W] Get Text    ${Element}
    [Return]    ${Text}

Add Addresses
    Set Library Search Order    SeleniumLibrary
    [W] Input Text    firstname    Wanchaloem
    [W] Input Text    lastname    Nanonchai
    [W] Input Text    company    Extosoft
    [W] Input Text    address1    Bangkok/36210
    [W] Input Text    address2    Bangkok/36210Bangkok/36210
    [W] Input Text    city    Bangkok
    [W] Select From List By Value    id_state    AA
    [W] Input Text    postcode    36210
    [W] Select From List By Value    id_country    United States
    [W] Input Text    phone    0912345678
    [W] Click Button    confirm-addresses
