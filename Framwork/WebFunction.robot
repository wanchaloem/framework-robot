*** Settings ***
Resource          AllResorces.robot

*** Keywords ***
[W] Open Browser
    [Arguments]    ${URL}
    Set Library Search Order    SeleniumLibrary
    Open Browser    ${URL}    chrome
    Maximize Browser Window

[W] Open Headless Browser
    [Arguments]    ${URL}
    Set Library Search Order    SeleniumLibrary
    Open Browser    ${URL}    headlesschrome
    Set Window Size    1920    1080

[W] Input Text
    [Arguments]    ${Element}    ${Text}
    Set Library Search Order    SeleniumLibrary
    Wait Until Page Contains Element    ${Element}
    Input Text    ${Element}    ${Text}

[W] Enter
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Press Keys    ${Element}    ENTER
    [Teardown]

[W] Click Element
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Wait Until Page Contains Element    ${Element}
    SeleniumLibrary.Click Element    ${Element}

[W] Get Text
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Wait Until Page Contains Element    ${Element}
    ${Return}    Get Text    ${Element}
    [Return]    ${Return}

[W] Log file Result
    [Arguments]    ${Folder}    ${fileName}    ${Text}
    Set Library Search Order    SeleniumLibrary
    ${Dir_File}    Remove String    ${CURDIR}    ${/}Framwork
    ${Dir_File}    Join Path    ${Dir_File}    ${Folder}    ${fileName}
    Append To File    ${Dir_File}    ${Text}\t    encoding=UTF16

[W] Wait Until Page Contains
    [Arguments]    ${Text}
    Set Library Search Order    SeleniumLibrary
    SeleniumLibrary.Capture Page Screenshot
    SeleniumLibrary.Wait Until Page Contains    ${Text}    15

[W] Click Button
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Wait Until Page Contains Element    ${Element}
    SeleniumLibrary.Click Element    ${Element}

[W] Click Image
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Wait Until Page Contains Element    ${Element}
    SeleniumLibrary.Click Element    ${Element}

[W] Select From List By Value
    [Arguments]    ${Element}    ${Message}
    Set Library Search Order    SeleniumLibrary
    Wait Until Page Contains Element    ${Element}
    ${Status}    ${Error}    Run Keyword And Ignore Error    Select From List By Label    ${Element}    ${Message}
    Run Keyword If    "${Status}"!="PASS"    Select From List By Value    ${Element}    ${Message}

[W] Click Link
    [Arguments]    ${URL}
    Set Library Search Order    SeleniumLibrary
    Click Link    ${URL}

[W] Select Radio Button
    [Arguments]    ${value}
    AppiumLibrary.Click Element    ${value}
