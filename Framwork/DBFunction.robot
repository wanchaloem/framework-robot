*** Settings ***
Resource          AllResorces.robot

*** Keywords ***
[DB] Connect to Database
    [Arguments]    ${DB_Name}    ${DB_Username}    ${DB_Password}    ${DB_Host}    ${DB_Port}
    Connect To Database    mysql.connector    ${DB_Name}    ${DB_Username}    ${DB_Password}    ${DB_Host}    ${DB_Port}

[DB] Insert Data to Database
    [Arguments]    ${name}    ${surname}    ${age}    ${Mobile}
    Execute SQL String    INSERT INTO `customer_profile`(`id`, `name`, `surname`, `age`, `mobile`) VALUES ('',"${name}","${surname}","${age}","${Mobile}")

[DB] Delete Data
    [Arguments]    ${Colum}    ${value}
    Execute SQL String    DELETE FROM `customer_profile` WHERE ${Colum} = '${value}'
    ${list}    Query    SELECT * FROM `customer_profile`
    [Return]    ${list}
