*** Settings ***
Resource          AllResorces.robot

*** Keywords ***
[Excel] Open Excel
    [Arguments]    ${Filename}    ${File_xlsx}    ${Sheet_Name}
    Set Library Search Order    ExcelRobot
    ${Filename}    Join Path    ${Filename}
    ${Dir_File}    Remove String    ${CURDIR}    ${/}Framwork
    ${Dir_File}    Join Path    ${Dir_File}    ${Filename}    ${File_xlsx}
    Open Excel    ${Dir_File}
    ${ColumnCount}    Get Column Count    ${Sheet_Name}
    ${ColumnRow}    Get Row Count    ${Sheet_Name}
    [Return]    ${ColumnCount}    ${ColumnRow}

[Excel] Read Call Data Colmn For Excel
    [Arguments]    ${Sheet_Name}    ${ColumnCount}    ${Row}
    Set Library Search Order    ExcelRobot
    ${ListData}    Create List
    FOR    ${I}    IN RANGE    ${ColumnCount}
        ${Return}    Read Cell Data    ${Sheet_Name}    ${I}    ${Row}
        Append To List    ${ListData}    ${Return}
        Set Suite Variable    ${ListData}
    END
    [Return]    ${ListData}

[Excel] Get Data All Row For Excal
    [Arguments]    ${Filename}    ${File_xlsx}    ${Sheet_Name}    ${Row}
    Set Library Search Order    ExcelRobot
    ${ColumnCount}    ${ColumnRow}    [Excel] Open Excel    ${Filename}    ${File_xlsx}    ${Sheet_Name}
    ${RowCount}    Evaluate    ${Row}-1
    FOR    ${I}    IN    ${RowCount}
        ${Return}    [Excel] Read Call Data Colmn For Excel    ${Sheet_Name}    ${ColumnCount}    ${I}
    END
    Insert Into List    ${Return}    0    ${EMPTY}
    [Return]    ${Return}

[Excel] Log file Result
    [Arguments]    ${Folder}    ${fileName}    ${Text}
    Set Library Search Order    SeleniumLibrary
    ${Dir_File}    Remove String    ${CURDIR}    ${/}Framwork
    ${Dir_File}    Join Path    ${Dir_File}    ${Folder}    ${fileName}
    Append To File    ${Dir_File}    ${Text}\t    encoding=UTF16
