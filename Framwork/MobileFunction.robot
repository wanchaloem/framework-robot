*** Settings ***
Resource          AllResorces.robot

*** Keywords ***
[M] Open Browser
    [Arguments]    ${URL}
    Set Library Search Order    SeleniumLibrary
    Open Browser    ${URL}    chrome
    Maximize Browser Window

[M] Open Headless Browser
    [Arguments]    ${URL}
    Set Library Search Order    SeleniumLibrary
    Open Browser    ${URL}    headlesschrome
    Set Window Size    1920    1080

[M] Input Text
    [Arguments]    ${Element}    ${Text}
    Set Library Search Order    SeleniumLibrary
    Input Text    ${Element}    ${Text}

[M] Enter
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    Press Keys    ${Element}    ENTER
    [Teardown]

[M] Click Element
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    SeleniumLibrary.Click Element    ${Element}

[M] Get Text
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    ${Return}    Get Text    ${Element}
    [Return]    ${Return}

[M] Log file Result
    [Arguments]    ${Folder}    ${fileName}    ${Text}
    Set Library Search Order    SeleniumLibrary
    ${Dir_File}    Remove String    ${CURDIR}    ${/}Framwork
    ${Dir_File}    Join Path    ${Dir_File}    ${Folder}    ${fileName}
    Append To File    ${Dir_File}    ${Text}\t    encoding=UTF16

[M] Wait Until Page Contains
    [Arguments]    ${Text}
    Set Library Search Order    SeleniumLibrary
    SeleniumLibrary.Capture Page Screenshot
    SeleniumLibrary.Wait Until Page Contains    ${Text}    15

[M] Click Button
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    SeleniumLibrary.Click Element    ${Element}

[M] Click Image
    [Arguments]    ${Element}
    Set Library Search Order    SeleniumLibrary
    SeleniumLibrary.Click Element    ${Element}
