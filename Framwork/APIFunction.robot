*** Settings ***
Resource          AllResorces.robot

*** Keywords ***
[API] GetAPI
    [Arguments]    ${URL_GET_API}    ${URI}    ${TextCheck}
    Create Session    URL    ${URL_GET_API}
    ${resp}    Get Request    URL    ${URI}
    Log To Console    ${resp.text}
    Should Contain    ${resp.text}    ${TextCheck}

[API] PostAPI
    [Arguments]    ${URL_POST}    ${email}    ${Password}
    Create Session    API    ${URL_POST}
    &{data}=    Create Dictionary    email=${email}    password=${Password}
    &{headers}=    Create Dictionary    Content-Type=application/json
    ${resp}=    POST Request    API    /api/login    data=${data}    headers=${headers}
    Log    ${resp.text}
